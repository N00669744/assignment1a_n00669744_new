﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="assignment1a_N00669744.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GroupUp</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Create A Team</h1>

            <label runat="server">Team Name: </label><asp:TextBox runat="server" ID="teamName" placeholder="Enter Team Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Team Name" ControlToValidate="teamName" ID="nameValidation"></asp:RequiredFieldValidator>
            <br />

            <label runat="server">Game Name: </label><asp:TextBox runat="server" ID="gameName" placeholder="Enter Game Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Game Name" ControlToValidate="gameName" ID="gameValidation"></asp:RequiredFieldValidator> 
            <br />
            <br />
            <label runat="server"># of Teammates:</label>
            <asp:DropDownList runat="server" ID="playerNum">
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
            </asp:DropDownList>
            <br />

            <div id="roleSelect">
                <label runat="server">Preferred Role(s): </label>
                <asp:CheckBox runat="server" ID="DPS" Text="DPS" />
                <asp:CheckBox runat="server" ID="Tank" Text="Tank" />
                <asp:CheckBox runat="server" ID="Healer" Text="Healer" />
             </div> 

            <div id="Platforms">
                <label runat="server">Gaming Platform: </label>
                <asp:RadioButton runat="server" Text="PC" GroupName="platform" ID="PC"  checked="true"/>
                <asp:RadioButton runat="server" Text="PS4" GroupName="platform" ID="PS4" />
                <asp:RadioButton runat="server" Text="Xbox One" GroupName="platform" ID="XB1" />
             </div>
            <br />

            <label runat="server">Voice Comms Required: </label> <asp:CheckBox runat="server" ID="voice" />
            <br />
            <br />

            <label runat="server">Group Type: </label>
            <asp:RadioButton runat="server" Text="Public" GroupName="groupType" checked="true"/>
            <asp:RadioButton runat="server" Text="Private" GroupName="groupType" />
            <br />

            <label runat="server">Password: </label><asp:TextBox runat="server" ID="pass" TextMode="Password"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="pass" ID="passCheck" ErrorMessage="Must be at least 5 characters" ValidationExpression="^[\s\S]{5,}$"></asp:RegularExpressionValidator>
            <br />
            <label runat="server">Confirm Password: </label><asp:TextBox runat="server" ID="passConfirm" TextMode="Password"></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToCompare="pass" ControlToValidate="passConfirm" ErrorMessage="Passwords do NOT match!"></asp:CompareValidator>
            <br />
            <br />
            <asp:ValidationSummary runat="server" />
            <asp:Button runat="server" ID="submit" type="submit" text="Submit"/>
        </div>
    </form>
</body>
</html>
